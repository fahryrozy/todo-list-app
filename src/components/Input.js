import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';

const {width, height} = Dimensions.get('window');

export const Input = ({leftIcon, placeholder, rightIcon, ...others}) => (
        <View style={styles.inputStyle}>
            {leftIcon}
            <TextInput placeholder={placeholder} style={{flex: 1}} {...others} />
            {rightIcon && (
                <TouchableOpacity>
                    {rightIcon}
                </TouchableOpacity>
            )}
        </View>
)


export const InputModal = ({leftIcon, placeholder, rightIcon, ...others}) => (
    <View style={styles.inputModalStyle}>
        {leftIcon}
        <TextInput placeholder={placeholder} style={{flex: 1}} {...others} />
        {rightIcon && (
            <TouchableOpacity>
                {rightIcon}
            </TouchableOpacity>
        )}
    </View>
)



const styles = StyleSheet.create({
    inputStyle: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        width: width*0.75,
        height: 56,
        borderRadius: 32,
        alignSelf: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
    },
    inputModalStyle: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        width: width*0.9,
        height: 56,
        backgroundColor: '#FFF',
        borderRadius: 15,
        alignSelf: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
    },
})
