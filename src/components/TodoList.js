import React, {useState, useEffect} from 'react'
import { View, Text, TouchableOpacity, FlatList, StyleSheet, Dimensions } from 'react-native'
import moment from "moment";
import { List } from 'react-native-paper';
import { Modal, Portal, Button, Dialog } from 'react-native-paper';
import { getAllTask, postTask, setCompleteTask, setActiveTask, deleteTask } from './../store/actions/taskAction';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector, useDispatch } from 'react-redux';

const { width, height } = Dimensions.get('window')

const TodoList = ({item}) => {
    const time = moment(item.createdAt, "YYYY-MM-DD h:mm:ss a").calendar()

    const [visible, setVisible] = React.useState(false);
    const showDialog = () => setVisible(true);
    const hideDialog = () => setVisible(false);
    const [delVisible, setDelVisible] = React.useState(false);
    const showDelDialog = () => setDelVisible(true);
    const hideDelDialog = () => setDelVisible(false);

    const token = useSelector((state) => state.authStore.payload.token)
    const dispatch = useDispatch();
    const completeToDoHandler = (_id, _token) => dispatch(setCompleteTask(_id, _token));
    const activateToDoHandler = (_id, _token) => dispatch(setActiveTask(_id, _token));
    const deleteToDoHandler = (_id, _token) => dispatch(deleteTask(_id, _token));

    console.log(visible);

    return (
        <View style={styles.listView}>
            <View style={styles.cardView}>
                <TouchableOpacity onPress={showDialog}>                
                    <List.Section>
                        <List.Item title={item.description} titleStyle={styles.title} left={() => 
                                <MaterialCommunityIcons name="git" size={25} style={{paddingVertical: 18, paddingLeft: 15, paddingRight: 5}} />
                        } />
                        {item.completed? (
                                <List.Item title="Completed" style={{marginVertical:-40}} titleStyle={styles.date} 
                                left={() => 
                                    <MaterialCommunityIcons name="checkbox-multiple-marked-outline" size={20} style={{padding: 18}} />} 

                                />
                            )
                            :
                            (
                                <List.Item title="Active" style={{marginVertical:-40}} titleStyle={styles.date} 
                                left={() => 
                                    <MaterialCommunityIcons name="checkbox-multiple-blank-outline" size={20} style={{padding: 18}} />} 

                                />
                            )
                        }       
                        <List.Item title={time} titleStyle={styles.date} left={() => <List.Icon icon="calendar" />} />
                    </List.Section>
                </TouchableOpacity>
                {item.completed? (              
                    <Portal>
                            <Dialog visible={visible} onDismiss={hideDialog}>
                            <Dialog.Title>Activate this todo list?</Dialog.Title>
                            <Dialog.Actions>
                                <Button onPress={hideDialog}>Cancel</Button>
                                <Button onPress={() => {
                                    activateToDoHandler(item._id, token)
                                    hideDialog()
                                }}>Activate</Button>
                            </Dialog.Actions>
                            </Dialog>
                        </Portal>
                    )
                    :
                    (                            
                        <Portal>
                            <Dialog visible={visible} onDismiss={hideDialog}>
                            <Dialog.Title>Complete this todo list?</Dialog.Title>
                            <Dialog.Actions>
                                <Button onPress={hideDialog}>Cancel</Button>
                                <Button onPress={() => {
                                    completeToDoHandler(item._id, token)
                                    hideDialog()
                                }}>Completed</Button>
                            </Dialog.Actions>
                            </Dialog>
                        </Portal>
                    )

                }
            </View>
            <View>                        
                <TouchableOpacity style={styles.deleteButton} onPress={showDelDialog}>                            
                    <MaterialCommunityIcons
                    name='delete'
                    size={24}
                    color='black'
                    style={{paddingLeft: 10}}
                    />
                </TouchableOpacity>        

                <Portal>
                    <Dialog visible={delVisible} onDismiss={hideDelDialog}>
                    <Dialog.Title>Delete todo list?</Dialog.Title>
                    <Dialog.Actions>
                        <Button onPress={hideDialog}>Cancel</Button>
                        <Button onPress={() => {
                            hideDelDialog()
                            deleteToDoHandler(item._id, token)
                        }}>Delete</Button>
                    </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        </View>
    )
}

export default TodoList


const styles = StyleSheet.create({
    listView: {
        width: width*0.9,
        flexDirection: 'row',
        backgroundColor: 'white',
        marginVertical: 5,
        borderRadius: width * 0.01,
        shadowColor: '#000',
        shadowOffset: { width:0.5, height: 0.5 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
        zIndex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardView: {
        width: width*0.7,
        backgroundColor: 'white',
        marginVertical: 5,
        shadowColor: '#000',
    },
    deleteButton: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width*0.2,
        paddingVertical: 50,
        // height: '100%',
    },
    title: {
        marginHorizontal: width * 0.02,
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold'
  
    },
    date: {
      fontSize: 12,
      color: 'gray'
    },
  })