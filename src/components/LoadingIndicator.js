import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, ActivityIndicator } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';

const {width, height} = Dimensions.get('window');

export const LoadingIndicator = () => (
        <View style={{flex: 1}}>
            <ActivityIndicator size="large" />
        </View>
)


const styles = StyleSheet.create({
    inputStyle: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        width: width*0.75,
        height: 56,
        borderRadius: 32,
        alignSelf: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
    },
    inputModalStyle: {
        marginVertical: 5,
        flexDirection: 'row',
        alignItems: 'center',
        width: width*0.9,
        height: 56,
        backgroundColor: '#FFF',
        borderRadius: 15,
        alignSelf: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
    },
})
