import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native'

const {width, height} = Dimensions.get('window');

export const OnboardImage = () => {
    const img = './../asset/images/OnboardTODO.jpg';
    return (
        <Image source={require(img)} style={styles.onboardImageStyle} />
    )
}

const styles = StyleSheet.create({
    onboardImageStyle: {
        width: width,
        height: height*0.6,
        resizeMode: 'contain',
    },
});