import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native'

const {width, height} = Dimensions.get('window');

export const TitleHeading = ({text}) => (
    <Text style={styles.titleHeadingStyle}>{text}</Text>
)

const styles = StyleSheet.create({
    titleHeadingStyle: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
        fontFamily: 'Inter',
        padding: 10,
    },
});