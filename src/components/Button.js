import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

const {width, height} = Dimensions.get('window');

export const PrimaryButton = ({title, leftIcon, style, ...others}) => (
    <TouchableOpacity {...others}>
        <View style={[styles.primaryButtonStyle, style]}>
            {leftIcon}
            <Text style={styles.textButton}>{title}</Text>
        </View>
    </TouchableOpacity>
)

export const EditButton = ({title, leftIcon, style, ...others}) => (
    <TouchableOpacity {...others}>
        <View style={[styles.editButtonStyle, style]}>
            {leftIcon}
            <Text style={styles.textButton}>{title}</Text>
        </View>
    </TouchableOpacity>
)

export const UnAvailButton = ({title, leftIcon, style, ...others}) => (
    <TouchableOpacity {...others}>
        <View style={[styles.unAvailButtonStyle, style]}>
            {leftIcon}
            <Text style={styles.unAvailButtonText}>{title}</Text>
        </View>
    </TouchableOpacity>
)

export const LinkButton = ({title, textStyle, ...others}) => (
    <TouchableOpacity {...others} >
        <View style={styles.linkButtonStyle}>
            <Text style={[styles.textLink, textStyle]}>{title}</Text>
        </View>
    </TouchableOpacity>
)

export const FloatingButton = ({title, textStyle, ...others}) => (
        <TouchableOpacity {...others} >
            <View style={styles.FloatingButtonStyle}>
                <MaterialCommunityIcons name="pencil-plus"  size={30} color="#01a699" />
            </View>
        </TouchableOpacity>
)

export const ModalButton = ({title, leftIcon, style, ...others}) => (
    <TouchableOpacity {...others}>
        <View style={[styles.modalButtonStyle, style]}>
            {leftIcon}
            <Text style={styles.textButton}>{title}</Text>
        </View>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    primaryButtonStyle: {
        flexDirection: 'row',
        backgroundColor: '#1FCC79',
        width: width*0.75,
        height: 56,
        paddingVertical: 15,
        borderRadius: 32,
        alignContent: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },    
    editButtonStyle: {
        flexDirection: 'row',
        backgroundColor: '#D0EEEE',
        width: width*0.35,
        height: 36,
        paddingVertical: 5,
        borderRadius: 12,
        alignContent: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },    
    modalButtonStyle: {
        flexDirection: 'row',
        backgroundColor: '#1FCC79',
        width: width*0.15,
        height: 56,
        paddingVertical: 15,
        borderRadius: 32,
        alignContent: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    }, 
    unAvailButtonStyle: {
        flexDirection: 'row',
        backgroundColor: 'transparent', 
        borderWidth: 1,
        borderColor: '#D0DBEA',
        width: width*0.75,
        height: 56,
        paddingVertical: 15,
        borderRadius: 32,
        alignContent: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },
    FloatingButtonStyle: {
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:70,
        position: 'absolute',                                          
        bottom: 30,                                                    
        right: 10,
        height:70,
        backgroundColor:'#fff',
        borderRadius:100,
        alignSelf: 'flex-end',
    },
    unAvailButtonText: {        
        fontSize: 15,
        color: '#D0DBEA',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    textButton: {
        fontSize: 15,
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    linkButtonStyle: {
        color: '#FFF',
    },
    textLink: {
        padding: 15,
        fontSize: 15,
        color: '#1FCC79',
        fontWeight: 'bold',
        textAlign: 'right',
    },
})
