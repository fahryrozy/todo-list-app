import React from 'react';
import { View, Text } from 'react-native'
import Navigation from "./navigation/Navigation";
import { Provider } from 'react-redux'
import store from "./store";
import { Provider as PaperProvider } from 'react-native-paper';

const App = () => {
  return (
    <Provider store={store}>
      <PaperProvider>
        <Navigation />
      </PaperProvider>
    </Provider>
    // <View>
    //   <Text>Test</Text>
    // </View>
  );
};

export default App;
