import React, {useState, useEffect} from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import Onboarding from '../screens/Onboarding'
import Login from './../screens/Login';
import Signup from './../screens/Signup';
import Splash from '../screens/Splash';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useSelector } from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Home from './../screens/Home';
import Profile from './../screens/Profile';
import Active from './../screens/Active';
import Complete from './../screens/Complete';
import { AsyncStorage } from '@react-native-async-storage/async-storage';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator()

const MainScreen = () => (
    <Tab.Navigator
      initialRouteName="Feed"
      tabBarOptions={{
        activeTintColor: '#1FCC79',
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'All',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="view-list-outline" color={color} size={size} />
          ),
        }}
      /><Tab.Screen
        name="Active"
        component={Active}
        options={{
          tabBarLabel: 'Active',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="credit-card-scan-outline" color={color} size={size} />
          ),
        }}
      /><Tab.Screen
        name="Completed"
        component={Complete}
        options={{
          tabBarLabel: 'Completed',
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="playlist-check" color={color} size={size} />
          ),
        }}
      /><Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="person" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
)

const Navigation = () => {
    
    const [isLoadingMainScreen, setisLoadingMainScreen] = useState(true);
    const token = useSelector((state) => state.authStore.payload.token)

    useEffect(()=>{
        setTimeout(()=>{
            setisLoadingMainScreen(false)
        }, 2000)
    }, [])

    if(isLoadingMainScreen) {
        return <Splash />
    }

    return (
        <NavigationContainer>
          {!token?(
            <Stack.Navigator headerMode="none" initialRouteName="Onboard">
                <Stack.Screen name="Onboard" component={Onboarding} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="SignUp" component={Signup} />
                <Stack.Screen name="Main" component={MainScreen} />
            </Stack.Navigator>
          ):
          (
            <Stack.Navigator headerMode="none" initialRouteName="Main">
                <Stack.Screen name="Main" component={MainScreen} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="SignUp" component={Signup} />
            </Stack.Navigator>
          )
          }
        </NavigationContainer>
    )
}

export default Navigation
