import fetchAxios from '../utils/fetchAxios';

export const login = async (email, password) => {
  try {
    const result = await fetchAxios('POST', 'user/login', {
      email,
      password
    });
    return result;
  } catch (error) {
    throw error;
  }
}

export const register = async (name, email, password, age) => {
  try {
    const result = await fetchAxios('POST', 'user/register', {
      name,
      email,
      password,
      age
    });
    return result;
  } catch (error) {
    throw error;
  }
}


export const logout = async (token) => {
  try {
    const result = await fetchAxios('POST', 'user/logout', {}, token);
    console.log(result);
    return result;
  } catch (error) {
    console.log(error)
    throw error;
  }
}

export const update = async (body, token) => {
  try {
    const result = await fetchAxios('PUT', 'user/me', body, token);
    console.log(result);
    return result;
  } catch (error) {
    throw error;
  }
}


export const deleteAccount = async (token) => {
  try {
    const result = await fetchAxios('PUT', 'user/me', {}, token);
    console.log(result);
    return result;
  } catch (error) {
    throw error;
  }
}

export const getAccount = async (token) => {
  try {
    const result = await fetchAxios('GET', 'user/me', {}, token);
    console.log(result);
    return result;
  } catch (error) {
    throw error;
  }
}
