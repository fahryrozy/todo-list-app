import fetchAxios from '../utils/fetchAxios';

export const getTODOs = async (token) => {
  try {
    const result = await fetchAxios('GET', 'task', {}, token);
    return result;
  } catch (error) {
    throw error;
  }
}

export const getCompletedTODOs = async (token) => {
  try {
    const result = await fetchAxios('GET', 'task?completed=true', {}, token);
    return result;
  } catch (error) {
    throw error;
  }
}

export const getActiveTODOs = async (token) => {
  try {
    const result = await fetchAxios('GET', 'task?completed=false', {}, token);
    return result;
  } catch (error) {
    throw error;
  }
}


export const postTODOs = async (body, token) => {
  try {
    const result = await fetchAxios('POST', 'task', body, token);
    return result;
  } catch (error) {
    throw error;
  }
}

export const updateTODOs = async (id, body, token) => {
  try {
    const result = await fetchAxios('PUT', `task/${id}`, body, token);
    return result;
  } catch (error) {
    throw error;
  }
}

export const deleteTODOs = async (id, token) => {
  try {
    const result = await fetchAxios('DELETE', `task/${id}`, {}, token);
    return result;
  } catch (error) {
    throw error;
  }
}
