import {getTODOs, getCompletedTODOs, postTODOs, updateTODOs, deleteTODOs} from '../../api/todoAPI';
import { getActiveTODOs } from './../../api/todoAPI';
export const getAllTask = (token) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_ALL_TASK_REQUEST'
  });

    try {
      console.log('Begin')
      const result = await getTODOs(token);
      if (result.status === 200) {
        
        dispatch({
          type: 'GET_ALL_TASK_SUCCESS',
          payload: result.data
        });
      } else {
        console.log('Failed')
        dispatch({
          type: 'GET_ALL_TASK_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      console.log('error')
      dispatch({
        type: 'GET_ALL_TASK_FAILED',
        error: err
      });
    }
  };
}
export const getCompletedTask = (token) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_COMPLETED_TASK_REQUEST'
  });

    try {
      console.log(token)
      const result = await getCompletedTODOs(token);
      if (result.status === 200) {
        console.log('result = '+ JSON.stringify(result))
        dispatch({
          type: 'GET_COMPLETED_TASK_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'GET_COMPLETED_TASK_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_COMPLETED_TASK_FAILED',
        error: err
      });
    }
  };
}

export const getActiveTask = (token) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_ACTIVE_TASK_REQUEST'
  });

    try {
      console.log(token)
      const result = await getActiveTODOs(token);
      if (result.status === 200) {
        console.log('result = '+ JSON.stringify(result))
        dispatch({
          type: 'GET_ACTIVE_TASK_SUCCESS',
          payload: result.data
        });
      } else {
        console.log('Failed')
        dispatch({
          type: 'GET_ACTIVE_TASK_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      console.log('error')
      dispatch({
        type: 'GET_ACTIVE_TASK_FAILED',
        error: err
      });
    }
  };
}

export const postTask = (body, token) => {
    return async (dispatch) => {
      dispatch({
        type: 'POST_TASK_REQUEST'
    });
  
      try {
        console.log(body, token)
        const result = await postTODOs(body, token);
        if (result.status === 201) {          
          dispatch({
            type: 'POST_TASK_SUCCESS',
            payload: result.data
          });
        } else {
          dispatch({
            type: 'POST_TASK_FAILED',
            error: result.data
          });
        }
      } catch (err) {
        dispatch({
          type: 'POST_TASK_FAILED',
          error: err
        });
      }
  };
}

export const setCompleteTask = (id, token) => {
  return async (dispatch) => {
    dispatch({
      type: 'UPDATE_TASK_REQUEST'
  });

    try {
      body = {completed: true}
      const result = await updateTODOs(id, body, token);
      if (result.status === 200) {
        dispatch({
          type: 'UPDATE_TASK_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'UPDATE_TASK_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'UPDATE_TASK_FAILED',
        error: err
      });
    }
};
}

export const setActiveTask = (id, token) => {
  return async (dispatch) => {
    dispatch({
      type: 'UPDATE_TASK_REQUEST'
  });

    try {
      body = {completed: false}
      const result = await updateTODOs(id, body, token);
      if (result.status === 200) {
        dispatch({
          type: 'UPDATE_TASK_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'UPDATE_TASK_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'UPDATE_TASK_FAILED',
        error: err
      });
    }
};
}


export const deleteTask = (id, token) => {
  return async (dispatch) => {
    dispatch({
      type: 'DELETE_TASK_REQUEST'
  });

    try {
      const result = await deleteTODOs(id, token);
      if (result.status === 200) {
        dispatch({
          type: 'DELETE_TASK_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'DELETE_TASK_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'DELETE_TASK_FAILED',
        error: err
      });
    }
};
}
