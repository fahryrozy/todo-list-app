import {login, logout, register, update, deleteAccount, getAccount} from '../../api/authApi';

export const fetchLogin = (email, password) => {
    return async (dispatch) => {
      dispatch({
        type: 'FETCH_LOGIN_REQUEST'
    });
  
      try {
        const result = await login(email, password);
        if (result.status === 200) {
          dispatch({
            type: 'FETCH_LOGIN_SUCCESS',
            payload: result.data
          });
        } else {
          dispatch({
            type: 'FETCH_LOGIN_FAILED',
            error: result.data
          });
        }
      } catch (err) {
        dispatch({
          type: 'FETCH_LOGIN_FAILED',
          error: err
        });
      }
  };
}

export const fetchRegister = (name, email, password, age, nav) => {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_REGISTER_REQUEST'
  });

    try {

      const result = await register(name, email, password, age);
      if (result.status === 201) {
        console.log(result);
        alert('Register Succeded')
        nav.navigate('Login');
        dispatch({
          type: 'FETCH_REGISTER_SUCCESS',
          payload: result.data
        });
      } else {
        console.log(result);
        alert('Register Failed')
        dispatch({
          type: 'FETCH_REGISTER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      alert('Register Failed')
      dispatch({
        type: 'FETCH_REGISTER_FAILED',
        error: err
      });
    }
  };
}

export const fetchUpdate = (body, token) => {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_UPDATE_REQUEST'
  });

    try {
      const result = await update(body, token);
      if (result.status === 200) {
        alert('Update Succeded')
        dispatch({
          type: 'FETCH_UPDATE_SUCCESS',
          payload: result.data
        });
      } else {
        alert('Update Failed')
        dispatch({
          type: 'FETCH_UPDATE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      alert('Update Failed')
      dispatch({
        type: 'FETCH_UPDATE_FAILED',
        error: err
      });
    }
};
}

export const fetchLogout = (token) => {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_LOGOUT_REQUEST'
  });

    try {
      const result = await logout(token);
      if (result.status === 200) {
        console.log('Logout Success');
        dispatch({
          type: 'FETCH_LOGOUT_SUCCESS',
          payload: result.data
        });
      } else {
        console.log('Logout Failed');
        dispatch({
          type: 'FETCH_LOGOUT_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      console.log('Logout Error');
      dispatch({
        type: 'FETCH_LOGOUT_FAILED',
        error: err
      });
    }
  };
}

export const fetchDelete = (token) => {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_DELETE_REQUEST'
  });

    try {
      const result = await deleteAccount(token);
      if (result.status === 200) {
        dispatch({
          type: 'FETCH_DELETE_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_DELETE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'FETCH_DELETE_FAILED',
        error: err
      });
    }
  };
}

export const fetchProfile = (token) => {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_PROFILE_REQUEST'
  });

    try {
      const result = await getAccount(token);
      if (result.status === 200) {
        dispatch({
          type: 'FETCH_PROFILE_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_PROFILE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'FETCH_PROFILE_FAILED',
        error: err
      });
    }
  };
}


