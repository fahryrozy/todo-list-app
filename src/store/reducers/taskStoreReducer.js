const defaultState = {
    todolist: {},
    completedlist: {},
    activelist: {},
    isLoading: false,
    isUpToDate: true,
  };
  
  export default (state = defaultState, action = {}) => {
    switch (action.type) {
      case 'GET_ALL_TASK_REQUEST': {
        return {
          ...state,
          isLoading: true,
        };
      }
      case 'GET_ALL_TASK_SUCCESS': {
        return {
          ...state,
          todolist: action.payload,
          isLoading: false,
          isUpToDate: true,
        };
      }
      case 'GET_ALL_TASK_FAILED': {
        return {
          ...state,
          isLoading: false,
        };
      }
      case 'GET_ACTIVE_TASK_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
      case 'GET_ACTIVE_TASK_SUCCESS': {
        return {
          ...state,
          activelist: action.payload,
          isLoading: false
        };
      }
      case 'GET_ACTIVE_TASK_FAILED': {
        return {
          ...state,
          isLoading: false
        };
      }
      case 'GET_COMPLETED_TASK_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
      case 'GET_COMPLETED_TASK_SUCCESS': {
        return {
          ...state,
          completedlist: action.payload,
          isLoading: false
        };
      }
      case 'GET_COMPLETED_TASK_FAILED': {
        return {
          ...state,
          isLoading: false
        };
      }
      case 'POST_TASK_REQUEST': {
        return {
          ...state,
          isLoading: true,
          isUpToDate: true,
        };
      }
      case 'POST_TASK_SUCCESS': {
        return {
          ...state,
          isLoading: false,
          isUpToDate: false,
        };
      }
      case 'POST_TASK_FAILED': {
        return {
          ...state,
          isLoading: false,
          isUpToDate: true,
        };
      }
      case 'UPDATE_TASK_REQUEST': {
        return {
          ...state,
          isLoading: true,
          isUpToDate: true,
        };
      }
      case 'UPDATE_TASK_SUCCESS': {
        return {
          ...state,
          isLoading: false,
          isUpToDate: false,
        };
      }
      case 'UPDATE_TASK_FAILED': {
        return {
          ...state,
          isLoading: false,
          isUpToDate: true,
        };
      }
      case 'DELETE_TASK_REQUEST': {
        return {
          ...state,
          isLoading: true,
          isUpToDate: true,
        };
      }
      case 'DELETE_TASK_SUCCESS': {
        return {
          ...state,
          isLoading: false,
          isUpToDate: false,
        };
      }
      case 'DELETE_TASK_FAILED': {
        return {
          ...state,
          isLoading: false,
          isUpToDate: true,
        };
      }
      default:
        return state;
    }
  };
  
  