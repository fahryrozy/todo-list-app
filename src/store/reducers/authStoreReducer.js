const defaultState = {
    payload: {},
    isLoading: false,
    isProfileUpdate: false,
    error: {},
    profile: {},
  };
  
  export default (state = defaultState, action = {}) => {
    switch (action.type) {
      case 'FETCH_LOGIN_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
  
      case 'FETCH_LOGIN_SUCCESS': {
        return {
          ...state,
          payload: action.payload,
          isLoading: false
        };
      }
  
      case 'FETCH_LOGIN_FAILED': {
        return {
          ...state,
          payload: {},
          error: action.error,
          isLoading: false
        };
      }
      
      case 'FETCH_LOGOUT_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
  
      case 'FETCH_LOGOUT_SUCCESS': {
        return {
          ...state,
          payload: {},
          isLoading: false
        };
      }
  
      case 'FETCH_LOGOUT_FAILED': {
        return {
          ...state,
          error: action.error,
          isLoading: false
        };
      }
      
      case 'FETCH_REGISTER_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
  
      case 'FETCH_REGISTER_SUCCESS': {
        return {
          ...state,
          isLoading: false
        };
      }
  
      case 'FETCH_REGISTER_FAILED': {
        return {
          ...state,
          error: action.error,
          isLoading: false
        };
      }
      
      case 'FETCH_UPDATE_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
  
      case 'FETCH_UPDATE_SUCCESS': {
        return {
          ...state,
          isProfileUpdate: true,
          isLoading: false
        };
      }
  
      case 'FETCH_UPDATE_FAILED': {
        return {
          ...state,
          error: action.error,
          isLoading: false
        };
      }
      
      case 'FETCH_DELETE_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
  
      case 'FETCH_DELETE_SUCCESS': {
        return {
          ...state,
          isLoading: false
        };
      }
  
      case 'FETCH_DELETE_FAILED': {
        return {
          ...state,
          error: action.error,
          isLoading: false
        };
      }
      
      case 'FETCH_PROFILE_REQUEST': {
        return {
          ...state,
          isLoading: true
        };
      }
  
      case 'FETCH_PROFILE_SUCCESS': {
        return {
          ...state,
          profile: action.payload,
          isLoading: false,
        };
      }
  
      case 'FETCH_PROFILE_FAILED': {
        return {
          ...state,
          error: action.error,
          isLoading: false
        };
      }
      default:
        return state;
    }
  };
  
  