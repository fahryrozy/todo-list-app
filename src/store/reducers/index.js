import { combineReducers } from 'redux';

import authStoreReducer from './authStoreReducer';
import taskStoreReducer from './taskStoreReducer';

const reducers = {
  authStore: authStoreReducer,
  taskStore: taskStoreReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
