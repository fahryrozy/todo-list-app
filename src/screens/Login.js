import React, { useState, useEffect } from 'react'
import { View, Text, Dimensions } from 'react-native'
import { TitleHeading } from '../components/Text'
import { Input } from '../components/Input';
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { PrimaryButton, LinkButton } from '../components/Button';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from 'react-redux';
import { fetchLogin } from '../store/actions/authAction';
import { ActivityIndicator, Colors } from 'react-native-paper';

const {width, height} = Dimensions.get('window');

const Login = ({navigation}) => {
    const [email, setEmail] = useState('Ted@gmail.com');
    const [password, setPassword] = useState('12345678');

    const dispatch = useDispatch();
    const loginHandler = (_email, _password) => dispatch(fetchLogin(_email, _password));

    const token = useSelector((state) => state.authStore.payload.token)

    const taskLoading = useSelector((state) => state.taskStore.isLoading)
    const progressLoading = useSelector((state) => state.authStore.isLoading)

    useEffect(() => {
        if (token && token !== null) {
            AsyncStorage.setItem('@token', token);
            navigation.replace('Main', {screen: 'Home'});
        }
      }, [token])
    
    if(progressLoading || taskLoading) {
        return(
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator animating={true} color={Colors.red800} />
            </View>
        )
    }
    
    return (
        <View style={{flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
            <View style={{flex: 2, alignItems: 'center', justifyContent: 'space-evenly'}}>
                <View>
                    <TitleHeading text="Welcome Back" />
                    <Text style={{textAlign: 'center', fontSize: 17, width: width, color: 'gray'}}>
                    Please enter your account here
                    </Text>
                </View>
                <View>
                    <Input
                        placeholder='Email address'
                        leftIcon={
                            <Ionicons
                            name='mail-outline'
                            size={24}
                            color='black'
                            style={{paddingRight: 10}}
                            />
                        }
                        onChangeText={(val)=>setEmail(val)}
                    />
                    <Input
                        placeholder='Password'
                        leftIcon={
                            <MaterialIcons
                            name='lock-outline'
                            size={24}
                            color='black'
                            style={{paddingRight: 10}}
                            />
                        }
                        rightIcon={                        
                            <Ionicons
                            name='eye-outline'
                            size={24}
                            color='black'
                            style={{paddingLeft: 10}}
                            />
                        }
                        onChangeText={(val)=>setPassword(val)}
                        secureTextEntry={true}
                    />
                    <LinkButton title="Forgot password?" 
                        style={{textAlign: 'right'}} 
                        textStyle={{color: 'black'}}
                        onPress={() => navigation.navigate('Password_Recovery')} 
                    />
                </View>
                
            </View> 
            
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', alignItems: 'center'}}>
                <View style={{flex: 0.6, alignItems: 'center', justifyContent: 'space-between', alignItems: 'center'}}>
                    <PrimaryButton title="Login"
                        leftIcon={                        
                            <MaterialCommunityIcons
                            name='login-variant'
                            size={20}
                            color='white'
                            style={{paddingRight: 10}}
                        />}
                        onPress={() => {
                            loginHandler(email, password)
                            }
                        }
                    />
                </View>
                <View style={{flex: 0.4, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Text>Don't have any account?</Text>
                    <LinkButton title="Sign Up" onPress={()=>navigation.navigate('SignUp')} />
                </View>
            </View>
        </View>
    )
}

export default Login
