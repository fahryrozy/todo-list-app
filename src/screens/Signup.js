import React, { useState } from 'react'
import { View, Text, Dimensions, TouchableOpacity } from 'react-native'
import { TitleHeading } from '../components/Text'
import { Input } from '../components/Input';
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { PrimaryButton } from '../components/Button';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector, useDispatch } from 'react-redux';
import { fetchRegister } from '../store/actions/authAction';

const {width, height} = Dimensions.get('window');

const Signup = ({navigation}) => {
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [age, setAge] = useState();
    const [isHidden, setIsHidden] = useState(true);
    
    const dispatch = useDispatch();
    const regsiterHandler = (_name, _email, _password, _age, _nav) => dispatch(fetchRegister(_name, _email, _password, _age, _nav));

    return (
        <View style={{flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
            <View style={{flex: 7, alignItems: 'center', justifyContent: 'space-evenly'}}>
                <View style={{flex: 2, justifyContent: 'flex-end', paddingVertical: 10,}}>
                    <TitleHeading text="Welcome" />
                    <Text style={{textAlign: 'center', fontSize: 17, width: width, color: 'gray'}}>
                    Please enter your account here
                    </Text>
                </View>
                <View style={{flex: 3, justifyContent: 'center'}}>
                    <Input
                        placeholder='Fullname'
                        name='name'
                        leftIcon={
                            <MaterialCommunityIcons
                            name='face-agent'
                            size={24}
                            color='black'
                            style={{paddingRight: 10}}
                            />
                        }
                        onChangeText={(val)=>setName(val)}
                    />
                    <Input
                        placeholder='Email or phone number'
                        name='email'
                        leftIcon={
                            <Ionicons
                            name='mail-outline'
                            size={24}
                            color='black'
                            style={{paddingRight: 10}}
                            />
                        }
                        onChangeText={(val)=>setEmail(val)}
                    />
                    <Input
                        secureTextEntry={isHidden}
                        placeholder='Password'
                        name='password'
                        leftIcon={
                            <MaterialIcons
                            name='lock-outline'
                            size={24}
                            color='black'
                            style={{paddingRight: 10}}
                            />
                        }
                        rightIcon={          
                            <TouchableOpacity onPress={()=>setIsHidden(false)}>
                                <Ionicons
                                    name='eye-outline'
                                    size={24}
                                    color='black'
                                    style={{paddingLeft: 10}}
                                />
                            </TouchableOpacity>      
                        }
                        onChangeText={(val)=>setPassword(val)}
                    />
                    <Input
                        placeholder='Age'
                        name='age'
                        leftIcon={
                            <MaterialCommunityIcons
                            name='timer-sand'
                            size={24}
                            color='black'
                            style={{paddingRight: 10}}
                            />
                        }
                        keyboardType="number-pad"
                        onChangeText={(val)=>setAge(val)}
                    />
                </View>
                <View style={{flex: 1, justifyContent: 'space-between', alignItems: 'flex-start'}}>
                    <Text>Your password must contain</Text>
                    {(password && password.toString().length>=6)?
                    (         
                        <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}> 
                            <Ionicons
                                name='checkmark-circle'
                                size={24}
                                color={'#1FCC79'}
                                style={{paddingRight: 15}}
                            />
                            <Text style={{color: '#000'}}>At least 6 characters</Text>
                        </View>
                    ):
                    (
                        <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>                      
                            <Ionicons
                                name='checkmark-circle'
                                size={24}
                                color={'#9FA5C0'}
                                style={{paddingRight: 15}}
                            />
                            <Text style={{color: '#9FA5C0'}}>At least 6 characters</Text>
                        </View>
                    )}
                    
                    
                    {(/\d/.test(password))?
                    (         
                        <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}> 
                            <Ionicons
                                name='checkmark-circle'
                                size={24}
                                color={'#1FCC79'}
                                style={{paddingRight: 15}}
                            />
                            <Text style={{color: '#000'}}>Contains a number</Text>
                        </View>
                    ):
                    (
                        <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>                      
                            <Ionicons
                                name='checkmark-circle'
                                size={24}
                                color={'#9FA5C0'}
                                style={{paddingRight: 15}}
                            />
                            <Text style={{color: '#9FA5C0'}}>Contains a number</Text>
                        </View>
                    )}
                </View>
                
            </View> 
            
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start', alignItems: 'center'}}>
                <PrimaryButton title="Sign Up" onPress={()=>regsiterHandler(name, email, password, age, navigation)} />
            </View>
        </View>
    )
}

export default Signup
