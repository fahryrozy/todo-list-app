import React, {useState, useEffect} from 'react'
import { View, Text, TouchableOpacity, FlatList, Dimensions } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector, useDispatch } from 'react-redux';
import { InputModal } from './../components/Input';
import { getAllTask, postTask } from './../store/actions/taskAction';
import { ActivityIndicator, Colors, Modal } from 'react-native-paper';
import TodoList from '../components/TodoList';

const { width, height } = Dimensions.get('window')

const Home = () => {
    const [description, setDescription] = useState()

    const todos = useSelector((state) => state.taskStore.todolist)
    const token = useSelector((state) => state.authStore.payload.token)
    const isUpToDate = useSelector((state) => state.taskStore.isUpToDate)
    const taskLoading = useSelector((state) => state.taskStore.isLoading)
    const progressLoading = useSelector((state) => state.authStore.isLoading)
    
    const [visible, setVisible] = useState(false);
    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    
    const dispatch = useDispatch();
    const addToDoHandler = (_body, _token) => dispatch(postTask(_body, _token));

    useEffect(() => {
        dispatch(getAllTask(token))
    }, [isUpToDate])

    if(progressLoading || taskLoading) {
        return(
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator animating={true} color={Colors.red800} />
            </View>
        )
    }

    if(!todos) {
        return(
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>Your data is still empty</Text>
            </View>            
        )
    }
    
    return (
        <View style={{flex: 1, alignItems: 'center', width: width}}>
            <FlatList data={todos.data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => (<TodoList item={item} />) }
                onEndReachedThreshold={0.5}

            />
            
        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={{backgroundColor: '#01a699', padding: 20, bottom: 0, position: 'absolute', zIndex: 20}} style={{justifyContent: 'flex-end'}}>
            <InputModal
                placeholder='Enter your TODO list'
                rightIcon={         
                    <TouchableOpacity onPress={()=>{
                        addToDoHandler(description, token);
                        hideModal();
                        }
                    }>                            
                        <MaterialCommunityIcons
                        name='send-check'
                        size={24}
                        color='black'
                        style={{paddingLeft: 10}}
                        />
                    </TouchableOpacity>           
                }
                onChangeText={(val)=>setDescription({description: val})}
            />
        </Modal>
        {!visible && 
            (
            <TouchableOpacity
            onPress={showModal}
            style={{
                borderWidth:1,
                borderColor:'rgba(0,0,0,0.2)',
                alignItems:'center',
                justifyContent:'center',
                width:70,
                position: 'absolute',                                          
                bottom: 20,                                                    
                right: 10,
                height:70,
                backgroundColor:'#01a699',
                borderRadius:50,
                }}
            >
                <MaterialCommunityIcons name="pencil-plus"  size={30} color="#FFF" />
            </TouchableOpacity>
            )
        }
        </View>
    )
}

export default Home
