import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet, SafeAreaView, TouchableOpacity, Dimensions, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { PrimaryButton, UnAvailButton, EditButton } from './../components/Button';
import { useSelector, useDispatch } from 'react-redux';
import { fetchLogout, fetchProfile, fetchUpdate } from '../store/actions/authAction'
import { getAllTask, getCompletedTask } from '../store/actions/taskAction';
import { ActivityIndicator, Colors, Modal } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { InputModal } from './../components/Input';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
 
const { width, height } = Dimensions.get('window')

const Profile = ({navigation}) => {
    const [user, setUser] = useState({})

    const completedTask = useSelector((state) => state.taskStore.completedlist)
    const allTask = useSelector((state) => state.taskStore.todolist)
    const profile = useSelector((state) => state.authStore.profile)
    const token = useSelector((state) => state.authStore.payload.token)
    const isProfileUpdate = useSelector((state) => state.authStore.isProfileUpdate)
    const taskLoading = useSelector((state) => state.taskStore.isLoading)
    const progressLoading = useSelector((state) => state.authStore.isLoading)
    const isUpToDate = useSelector((state) => state.taskStore.isUpToDate)
    
    const [visible, setVisible] = useState(false);
    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);

    const dispatch = useDispatch();
    const updateProfile = (_user, _token) => dispatch(fetchUpdate(_user, _token))
    const logout = (_token) => dispatch(fetchLogout(_token));
    
    useEffect(() => {          
        dispatch(fetchProfile(token));
        dispatch(getAllTask(token));
        dispatch(getCompletedTask(token));
    }, [isProfileUpdate, isUpToDate]);

    useEffect(() => {
        if (!token) {
          console.log(`before ${token}`);
          AsyncStorage.clear();
          navigation.replace('Login');
        }   
    }, [token])

    if(progressLoading || taskLoading) {
      return(
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <ActivityIndicator animating={true} color={Colors.red800} />
          </View>
      )
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.topContainer}>
                <Ionicons name="person-circle" size={75} color="#EFEFEF" />
                <Text style={styles.title}>{profile.name}</Text>
                <Text style={styles.text}>{profile.age} years old</Text>
                <EditButton title="Edit Profile" onPress={showModal} />
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center'}}>
                  <Text style={styles.label}>All Task</Text>
                  <Text style={styles.description}>{allTask.count}</Text>
                </View>                
                <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center'}}>
                  <Text style={styles.label}>Completed Task</Text>
                  <Text style={styles.description}>{completedTask.count}</Text>
                </View>
            </View>
            <View style={styles.bottomContainer}>
                <PrimaryButton onPress={()=>{logout(token)}} title="Logout" />
                <UnAvailButton title="Delete Account" onPress={()=>alert('You cannot delete this account')} />
            </View>

            <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={{background: '#FFF', bottom: 0, position: 'absolute', zIndex: 20}} style={{justifyContent: 'center', alignItems: 'center'}}>
                <View style={{backgroundColor: '#FFF', width: width, alignItems: 'center'}}>
                  <Text style={styles.title}>Update your profile</Text>
                  <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width:width*0.9}}>
                    <Text style={{width: width*0.2}}>Name : </Text>
                    <TextInput placeholder="Enter your name here" style={{borderBottomWidth: 1, paddingHorizontal: 10, width: width*0.5}}
                        onChangeText={(val)=>setUser({...user, name: val})}
                    />
                  </View>
                  <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center',  width: width*0.9, marginBottom: 85}}>
                    <Text style={{width: width*0.2}}>Age : </Text>
                    <TextInput placeholder="Enter your name here" style={{borderBottomWidth: 1, paddingHorizontal: 10, width: width*0.5}}
                      onChangeText={(val)=>setUser({...user, age: val})}
                  />
                  </View>
                  <PrimaryButton title="Update" onPress={()=>updateProfile(user, token)} style={{marginBottom: 50, borderRadius: 12, width: width*0.5}} />
                </View>
            </Modal>
        </SafeAreaView>
    )
}

export default Profile;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    topContainer: {
      alignItems: 'center',
      flex: 2,
      width: '100%',
      marginVertical: '20%',
    },    
    bottomContainer: {
      alignItems: 'center',
      justifyContent: 'flex-start',
      flex: 2,
      width: width,
      borderRadius: 10,
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginVertical: 10,
    },   
    label: {
      color: '#003366',
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'center',
      fontFamily: 'Roboto',
    },
    description: {
      color: '#003366',
      fontSize: 14,
      fontWeight: 'bold',
      textAlign: 'center',
      fontFamily: 'Roboto',
    },
    title: {
      color: '#003366',
      fontSize: 22,
      fontWeight: 'bold',
      textAlign: 'center',
      marginVertical: 20,
    },
    text: {
      color: '#3EC6FF',
      fontSize: 14,
      fontWeight: 'bold',
      textAlign: 'center',
    },
  });
  