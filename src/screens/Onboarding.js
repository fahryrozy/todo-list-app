import React from 'react'
import { View, Text } from 'react-native'
import { PrimaryButton } from '../components/Button';
import { TitleHeading } from '../components/Text';
import { OnboardImage } from './../components/Image';

const Onboarding = ({navigation}) => {
    return (
        <View style={{flex: 1, justifyContent: 'space-between', alignItems: 'center', backgroundColor: '#FFF'}}>
            <View style={{flex: 3, alignItems: 'center', justifyContent: 'space-between'}}>
                <OnboardImage />
                <TitleHeading text="Stay Productive" />
                <Text style={{textAlign: 'center', fontSize: 17, width: 300, color: 'gray'}}>
                Let’s join our community to make your to do list cleaner
                </Text>
            </View>
            <View style={{flex: 1, justifyContent: 'center'}}>
                <PrimaryButton title="Get Started" onPress={()=>navigation.navigate('Login')} />
            </View>
        </View>
    )
}

export default Onboarding
