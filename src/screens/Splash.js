import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const Splash = ({navigation}) => {
    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
           <Image
        style={styles.background}
        source={require('./../asset/background/coffee-list.jpg')}
      />
        </View>
    )
}

export default Splash;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        resizeMode: "contain",
        alignItems: 'center'
      },
})
