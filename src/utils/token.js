import AsyncStorage from '@react-native-async-storage/async-storage';

export const getToken = async () => {
    try {
      const value = await AsyncStorage.getItem('@token');
      if (value !== null) {
        console.log('token = '+JSON.stringify(value));
        console.log(value)
        return value;
      }
      else {
        return null
      }
    } catch (error) {
      console.log(error);
      return null;
    }
}

export const saveToken = async (value) => {
  try {
    await AsyncStorage.setItem('@token', value)
  } catch (e) {
    // saving error
  }
}